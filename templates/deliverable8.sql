DROP TABLE IF EXISTS `Chromosomes` ;

CREATE TABLE IF NOT EXISTS `Chromosomes` (
  `chromosome` VARCHAR(45) NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`));


-- -----------------------------------------------------
-- Table `mydb`.`Genes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Genes` ;

CREATE TABLE IF NOT EXISTS `Genes` (
  `RefSeq_Gene` VARCHAR(45) NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `chr_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`chr_id`)
    REFERENCES `Chromosomes` (`id`)
    );


-- -----------------------------------------------------
-- Table `mydb`.`Variants`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Variants` ;

CREATE TABLE IF NOT EXISTS `Variants` (
  `begin` INT NOT NULL,
  `end` INT NOT NULL,
  `reference` CHAR(1) NOT NULL,
  `observed` CHAR(1) NOT NULL,
  `RefSeq_Func` VARCHAR(45) NULL,
  `dbsnp138` VARCHAR(45) NULL,
  `c1000g2015aug_EUR` INT NULL DEFAULT '',
  `LJB2_SIFT` INT NULL,
  `LJB2_PolyPhen2_HDIV` VARCHAR(45) NULL,
  `LJB2_PolyPhen2_HVAR` VARCHAR(45) NULL,
  `CLINVAR` VARCHAR(45) NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `gene_id` INT NOT NULL,
  PRIMARY KEY (`id`),
    FOREIGN KEY (`gene_id`)
    REFERENCES `Genes` (`id`)
);
