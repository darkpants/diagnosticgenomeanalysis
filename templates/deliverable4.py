#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for a program performing the following steps:
----------------------------------------------------------
* Load the BED file containing the information (names, chromosome and
  coordinates of the exons) of all cardiopanel genes
* Load the pileup file containing the mapping data
* For each exon found in the BED file:
    * read the start- and end-coordinate
    * find all entries in the pileup file for this chromosome and within
      these coordinates
    * for each pileup-entry:
        * store the coverage (data from column 4)
* Given the found coverage for each position in all exons:
    * Calculate the average coverage per gene
    * Count the number of positions with a coverage < 30
* Write a report on all findings (output to Excel-like file)

Deliverable 4
-------------
This template contains a number of placeholders where you are asked to place
your own code made in previous deliverables, following the instructions
preceded with double '##' symbols.

The 'main()' functions glues all your functions into a single coherent
program that performs all required steps.

* Note: Test the program on the example data first.
* Note: by default the 'data/example.bed' and 'data/example.pileup' files are
        used as input, but you can supply your own files on the
        commandline.

    usage:
        python3 deliverable4.py [bed-file.bed] [pileup-file.pileup]
"""

# METADATA VARIABLES
__author__ = "Hugo Donkerbroek & Stijn Arends"
__version__ = "2018.d4.v1"

# IMPORT
import sys
import csv

# FUNCTIONS
def read_data(filename):
    """ This function reads in data and returns a list containing one
        line per element. """
    
    ## Open the file given the filename stored in 'filename'
    bed_list = []
    open_file = open(filename)
    for line in open_file:
        line = line.strip('\n')
        bed_list.append(line)

    ## Return a list where each line is a list element
    return bed_list


def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """
    
    ## Create empty dictionary to hold the data
    bed_dict = {}
        
    ## Iterate over all lines in the 'bed_data' list and fill the
    ## `bed_dict` dictionary with the `chromosome` as key. The other fields
    ## are added as a tuple using the correct types.
    ## Check the `expected_bed_dict` output example in the `main` function below.
    for i in bed_data:
        clean_data = i.split('\t')
        clean_data[1] = int(clean_data[1])
        clean_data[2] = int(clean_data[2])
        key = clean_data[0]
        value = clean_data[1:]
        if key not in bed_dict:
            bed_dict[key] = [tuple(value)]
        else:
            bed_dict[key].append(tuple(value))

    ## Return the bed_dict one all lines are done
    return bed_dict


def parse_pileup_data(pileup_data, bed_dict):
    """ Function that parses pileup data and collects the per-base coverage
        of all exons contained in the BED data.

        Iterate over all pileup lines and for each line:
        - check if the position falls within an exon (from `bed_dict`)
        - if so; add the coverage to the `coverage_dict` for the correct gene
    """

    ## Create empty dictionary to hold the data
    coverage_dict = {}

    ## Iterate over all the lines contained in the pileup_data        
    ## Extract the 'chromosome' field and remove the 'chr' text
    ## Check if the chromosome is contained in the bed_dict
        ## If yes; extract the coordinate from the pileup and compare to all
        ##         exons for that chromosome in the `bed_dict`
        ##         If the position falls within an exon, add the coverage
        ##         value to the list for the gene in the `coverage_dict`

    for i in pileup_data:
        clean_data = i.split('\t')
        chrs = clean_data[0][3:]

        if chrs in bed_dict.keys():
            coord = int(clean_data[1])
            cov_val = int(clean_data[3])

            for t in bed_dict[chrs]:

                if coord >= t[0] and coord <= t[1]:
                    key = t[2]
                    value = cov_val

                    if key not in coverage_dict:
                        coverage_dict[key] = [value]
                    else:
                        coverage_dict[key].append(value)

    ## Return coverage dictionary
    return coverage_dict


def calculate_mapping_coverage(coverage_dict):
    """ Function to calculate all coverage statistics on a per-gene basis
        and store this in a list.
        Note: this function is taken from deliverable 5 and slightly modified
    """

    ## Create an empty list that will hold all data to save
    statistics = []

    ## Iterate over all the genes in the coverage_dict getting the gene name
    ## and list with coverage data for that gene

    ## Put the following elements in a single tuple and append to the
    ## statistics list.
    ##      * Gene name,
    ##      * Total positions (gene length covered)
    ##      * Average Coverage (use round with one position)
    ##      * Number of low-coverage positions (coverage value < 30)

    for i in coverage_dict:
        count = 0
        gene_name = i
        tot_positions = len(coverage_dict[i])
        avg = round(sum(coverage_dict[i])/len(coverage_dict[i]), 1)
        for v in coverage_dict[i]:
            if v < 30:
                count = count + 1
        statistics.append((gene_name, tot_positions, avg, count))
    
    ## Return the list of tuples holding the data
    return statistics


def save_coverage_statistics(coverage_file, coverage_statistics):
    """ Writes coverage data to a tabular file using Python's
        csv library: https://docs.python.org/3/library/csv.html#csv.writer
    """

    # Write the coverage_statistics to a CSV file
    
    with open('d4_output.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter = '\t', quotechar = '|', quoting=csv.QUOTE_MINIMAL)

        for i in coverage_statistics:
            spamwriter.writerow(i)


######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function connecting all functions
        Note: the 'is None' checks that are done are only
        necessary for this program to run without error if
        not all functions are completed.
    """

    ### INPUT ###
    # Try to read input en output filenames from the commandline. Use defaults if
    # they are missing and warn if the extensions are 'wrong'.
    if len(args) > 1:
        bed_file = args[1]
        if not bed_file.lower().endswith('.bed'):
            print('Warning: given BED file does not have a ".bed" extension.')
        pileup_file = args[2]
        if not pileup_file.lower().endswith('.pileup'):
            print('Warning: given pileup file does not have a ".pileup" extension.')
        output_file = args[3]
    else:
        bed_file = 'example.bed'
        pileup_file = 'example.pileup'
        output_file = 'd4_output.csv'

    # STEP 1: Read BED data
    print('Reading BED data from', bed_file)
    bed_data = read_data(bed_file)
    if bed_data is None:
        print('No BED-data read...')
    else:
        print('\t> A total of', len(bed_data), 'lines have been read.\n')

    # STEP 2: Read Pileup data
    print('Reading pileup data from', pileup_file)
    pileup_data = read_data(pileup_file)
    if pileup_data is None:
        print('No Pileup-data read...')
    else:
        print('\t> A total of', len(pileup_data), 'lines have been read.\n')

    # STEP 3: Parsing BED data
    print('Parsing BED data...')
    bed_dict = parse_bed_data(bed_data)
    if bed_dict is None:
        print('BED-data not parsed!')
    else:
        print('\t> A total of', len(bed_dict.keys()), 'chromosomes have been stored.\n')

    # STEP 4: Parsing and filtering pileup data
    print('Parsing and filtering pileup-data...')
    coverage_dict = parse_pileup_data(pileup_data, bed_dict)
    if coverage_dict is None:
        print('Pileup data not parsed!')
    else:
        print('\t> Coverage of', len(coverage_dict.keys()), 'genes have been stored.\n')

    # STEP 5: Store calculated data
    print('Calculating coverage statistics...')
    coverage_statistics = calculate_mapping_coverage(coverage_dict)
    if coverage_statistics is None:
        print('No coverage statistics calculated!')
    else:
        print('\t> Statistics for', len(coverage_statistics), 'genes have been calculated.\n')

    # STEP 6: Write output data
    print('Writing the coverage statistics to', output_file)
    if coverage_statistics is None:
        print('Nothing to write, quitting...')
    else:
        save_coverage_statistics(output_file, coverage_statistics)
        from pathlib import Path
        csv_file_check = Path(output_file)
        if csv_file_check.is_file():
            print('\t> CSV file created, program finished.')
        else:
            print('\tCSV file', output_file, 'does not exist!')

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
