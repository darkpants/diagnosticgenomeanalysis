#!/usr/bin/env python3

import argparse

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Template for parsing and filtering VCF data given a certain variant
allele frequency value.

Deliverable 8
-------------
Make changes to the `parse_vcf_data` function AND the `main` function,
following the instructions preceded with double '##' symbols.

    usage:
        python3 deliverable8.py vcf_file.vcf frequency out_file.vcf

    arguments:
        vcf_file.vcf: the input VCF file, output from the varscan tool
                      frequency: a number (integer) to use as filtering value
        out_file.vcf: name of the output VCF file 

    output:
        a VCF file containing the complete header (comment lines) and
        only the remaining variant positions after filtering.
"""

# METADATA VARIABLES
__author__ = "Hugo Donkerbroek & Stijn Arends"
__version__ = "2017.d8.v3"

# IMPORT
import sys
import argparse

#FUNCTIONS
def parse_vcf_data(vcf_input_file, frequency, vcf_output_file):
    """ This function reads the input VCF file line by line, skipping the first
    n-header lines. The remaining lines are parsed to filter out variant allele
    frequencies > frequency.
    """

    ## Open the INTPUT VCF file, read the contents line-by-line
    ## Write the first ... comment-lines (header) directly to the output file
    with open(vcf_input_file) as vcf_info:
        with open(vcf_output_file, 'w') as vcf_out:
            for line in vcf_info:
                if line.startswith('#'):
                    vcf_out.write(line)
                else:
                    sep_line = line.split()
                    number = sep_line[-1].split(':')
                    FREQ = float(number[6].strip('%'))
                    ## Compare the 'FREQ' field with the `frequency` value and write the line
                    ## to the output file if FREQ > frequency
                    if FREQ > frequency:
                        vcf_out.write(line)

    pass


# MAIN
def main(args):
    """ Main function """

    ### INPUT ###
    # Try to read input arguments from the commandline.
    # *After* testing, make sure the program gives proper errors if input is missing
    parser = argparse.ArgumentParser(description='VCF File Filtering.')
    parser.add_argument('vcf_file',
                        help = 'The name of the input vcf file')
    parser.add_argument('frequency', default = 30, type = float, nargs = '?',
                        help = 'the variant allele frequency (default = 30)')
    parser.add_argument('out_vcf',
                        help = 'the name of the output file')

    args = parser.parse_args()

    # Process the VCF-file
    parse_vcf_data(args.vcf_file, args.frequency, args.out_vcf)

    return 0


if __name__ == "__main__":
    sys.exit(main(sys.argv))
