#!/usr/bin/env python3

"""
BFV2 Theme 05 - Genomics - Sequencing Project

Simple template for parsing BED data.

Deliverable 1
-------------
Make changes to the 'parse_bed_data' function, following the instructions
preceded with double '##' symbols.

    usage:
        python3 deliverable1.py
"""

# METADATA VARIABLES
__author__ = "Marcel Kempenaar"
__status__ = "Template"
__version__ = "2018.d1.v1"

# IMPORT
import sys

# FUNCTIONS
def parse_bed_data(bed_data):
    """ Function that parses BED data and stores its contents
        in a dictionary
    """
    ## Create empty dictionary to hold the data
    bed_dict = {}

    #not working yet, a work in progress
    for i in bed_data:
        #select the first 2 characters and delete the tab after
        if i[0:2].rstrip() not in bed_dict:
            #updating the dictionary with the key:value pair
            #the ':20' is a placeholder value for the key in the dictionary
            bed_dict.update({x[0:2].rstrip():20})


    print(bed_dict)
    ## Iterate over all lines in the 'bed_data' list and fill the
    ## `bed_dict` dictionary with the `chromosome` as key. The other fields
    ## are added as a tuple using the correct types.
    ## Check the `expected_bed_dict` output example in the `main` function below.
    ## Return the bed_dict one all lines are done
    return bed_dict

######
# Do not change anything below this line
######

# MAIN
def main(args):
    """ Main function that tests for correct parsing of BED data """
    ### INPUT ###
    bed_data = [
        "1	237729847	237730095	RYR2",
        "1	237732425	237732639	RYR2",
        "1	237753073	237753321	RYR2",
        "18	28651551	28651827	DSC2",
        "18	28654629	28654893	DSC2",
        "18	28659793	28659975	DSC2",
        "X	153648351	153648623	TAZ",
        "X	153648977	153649094	TAZ",
        "X	153649222	153649363	TAZ"
    ]

     # Call the parse-function
    bed_dict = parse_bed_data(bed_data)
    

if __name__ == '__main__':
    sys.exit(main(sys.argv))
