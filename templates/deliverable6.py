#!/usr/bin/env python3
'''
Deliverable6
'''

__author__ = 'Hugo Donkerbroek & Stijn Arends'

#METADATA
import sys
import argparse
from operator import itemgetter

#FUNCTIONS
def read_file(filename):
    '''
    Parses the file, filtering out the header (header) and the values
    (line) for the database. calls get_columns(line, header).
    '''
    header = ''
    content = []
    
    with open(filename) as op:
        for line in op:
            if line.startswith('chromosome'):
                header = line
                header = header.split('\t')
                header = ['c' + x if x.startswith('1000') else x for x in header]
            else:
                line = line.strip('\n')
                line = line.split('\t')
                content.append(get_columns(line, header))
                
    return content


def get_columns(line, header):
    '''
    Use itemgetter to combine the desired columns(header) with the coherent data(line)

    calls get_gene_name with the gene name column so the name of the gene
    can be changed.
    '''

    columns = [*range(0, 5), 15, 16, 27, *range(33, 37), -1]
    q = dict(zip(itemgetter(*columns)(header), itemgetter(*columns)(line)))
    print(q)
    
    return q


#MAIN
def main(args):

    parser = argparse.ArgumentParser()
    parser.add_argument('ANNOVAR_file',
                        default = '/data/example.txt',
                        help = 'give an input file')
    
    args = parser.parse_args()

    content = read_file(args.ANNOVAR_file)

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))
