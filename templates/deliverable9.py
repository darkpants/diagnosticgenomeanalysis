#!/usr/bin/env python3
'''
this file is used for inserting the values out of a ANNOVAR file into a database
'''

# METADATA VARIABLES
__author__ = "Hugo Donkerbroek & Stijn Arends"

# IMPORT
import sys
import mysql.connector
import argparse
from operator import itemgetter
import re

# FUNCTIONS
def check_annovar_file(ANNOVAR_file):
    '''
    checks if the ANNOVAR file exists and if it contains tab-separated data,
    if both are true it returns 1, if either one
    of them is false, return 0.
    '''
    try:
        with open(ANNOVAR_file) as op:
            for line in op:
                if line.startswith('chromosome'):
                    line = line.split('\t')
                    if len(line) > 1:
                        return 1
                    else:
                        return 0
    except:
        return 0


def database_connector(database, username, password):
    '''
    connects the program to the MySQL server
    '''
    try:
        cnx = mysql.connector.connect(user = username, password = password,
                                      host = 'mysql.bin',
                                      database = database)
        return cnx

    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Something is wrong with your user name or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)


def create_tables(cursor):
    '''
    creates new, empty tables and drops existing ones
    '''
    drop_tables = """
    DROP TABLE IF EXISTS Variants ;
    DROP TABLE IF EXISTS Genes ;
    DROP TABLE IF EXISTS Chromosomes ;
    """
    
    chromosome_table = """
    CREATE TABLE IF NOT EXISTS Chromosomes (
        chromosome VARCHAR(45) NOT NULL, 
        id INT NOT NULL AUTO_INCREMENT,
        PRIMARY KEY (id));
    """

    gene_table = """
    CREATE TABLE IF NOT EXISTS Genes (
        RefSeq_Gene VARCHAR(45) NOT NULL,
        id INT NOT NULL AUTO_INCREMENT,
        chr_id INT NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (chr_id)
        REFERENCES Chromosomes (id)
    );"""

    variant_table = """
    CREATE TABLE IF NOT EXISTS Variants (
        begin INT NOT NULL,
        end INT NOT NULL,
        reference CHAR(1) NOT NULL,
        observed CHAR(1) NOT NULL,
        RefSeq_Func VARCHAR(45) NULL,
        dbsnp138 VARCHAR(45) NULL,
        c1000g2015aug_EUR INT NULL,
        LJB2_SIFT INT NULL,
        LJB2_PolyPhen2_HDIV VARCHAR(45) NULL,
        LJB2_PolyPhen2_HVAR VARCHAR(45) NULL,
        CLINVAR VARCHAR(45) NULL,
        id INT NOT NULL AUTO_INCREMENT,
        gene_id INT NOT NULL,
        PRIMARY KEY (id),
          FOREIGN KEY (gene_id)
          REFERENCES Genes (id)
    );"""

    for q in cursor.execute(drop_tables, multi = True):
        continue

    cursor.execute(chromosome_table)
    cursor.execute(gene_table)
    cursor.execute(variant_table)
    

def read_annovar_file(annovar_file):
    '''
    reads and parses the file, filtering out the header (header) and the values
    (line) for the database. calls get_columns(line, header).

    returns three lists of values for the 3 tables in the database
    '''
    header = ''
    variant_l = []
    gene_l = []
    chro_l = []
    
    with open(annovar_file) as op:
        for line in op:
            if line.startswith('chromosome'):
                header = line
                header = header.split('\t')
                header = ['c' + x if x.startswith('1000') else x for x in header]
            else:
                line = line.strip('\n')
                line = line.split('\t')
                [variant, gene, chro] = get_columns(line, header)
                variant_l.append(variant)
                gene_l.append(gene)
                chro_l.append(chro)

    return variant_l, gene_l, chro_l


def get_columns(line, header):
    '''
    Use itemgetter to combine the desired columns(header) with the coherent data(line)

    calls get_gene_name with the gene name column so the name of the gene
    can be changed.

    return three dictionaries with the values to the read_annovar_file function so the
    values can be returned
    '''
    v_columns = [*range(1, 5), 15, 27, *range(33, 37), -1]
    variant = dict(zip(itemgetter(*v_columns)(header), itemgetter(*v_columns)(line)))


    g_columns = [16, 16]
    gene = dict(zip(itemgetter(*g_columns)(header), itemgetter(*g_columns)(line)))

    c_columns = [0, 0]
    chro = dict(zip(itemgetter(*c_columns)(header), itemgetter(*c_columns)(line)))

    #update the gene dictionary with the correct gene name
    gene['RefSeq_Gene'] = get_gene_name(gene)

    return variant, gene, chro
    

def get_gene_name(gene_name_dict):
    '''
    This function returns the gene name from a complex 'RefSeq_Gene' field
    from the insert_values function.
    'LOC****' (Uncharacterized Locus),
    'LINC****' (Long Intergenic Non-Coding RNA segments) and
    'NONE' elements are filtered out.

    If a record contains multiple gene-names (incase of an intergenic variant),
    combine these genes with a '/' delimeter, i.e.: 'BIN1/CYP27C1'

    If a record only contains 'NONE', 'LOC' or 'LIN' elements, the gene name
    becomes a '-'.

    Input is a single RefSeq_Gene record (Dictionary) and the output is a single
    string with the filtered gene name.
    '''
    for key in gene_name_dict:
        gene_name = gene_name_dict[key]
        gene = ''
    
        #REEE expression
        s = '[A-Z]{1,4}[0-9]*[A-Z]*[0-9]*'
        # Process ...    
        if ',' in gene_name:
            split_gene = gene_name.split(',')
            new_g = []
            for gene in split_gene:
                gene = re.findall(s, gene)
                if gene[0].startswith('LOC') or gene[0].startswith('LIN') or gene[0].startswith('NONE'):
                    continue
                else:
                    new_g.append(gene[0])
            if len(new_g) == 0:
                return '-'
            gene = '/'.join(new_g)
        else:
            gene = re.match(s, gene_name).group()
            if gene.startswith('LOC') or gene.startswith('LIN') or gene.startswith('NONE'):
                return '-'
            else:
                return gene
        return gene


def insert_into_table(table, data, cursor):
    """ Generic function that, given a table name and data-dictionary,
    inserts a single row into a table, returning the ID field value. """

    query = "INSERT INTO {} ({}) VALUES ({})".format(table,
                                                     # List the column names
                                                     ', '.join(data.keys()),
                                                     # Format the VALUES as '(%(name)s, %(chr_id)s)'
                                                     ', '.join(['%({})s'.format(k) for k in data]))
    cursor.execute(query, data)
    # Get the ID field value (primary key) of the inserted row
    return cursor.lastrowid


def fill_tables(variant, gene, chro, cursor):
    n_items = len(chro)

    # Process data 'line-by-line'
    for i in range(n_items):
        # Insert into chromosome table, retrieving and storing ID
        gene[i]['chr_id'] = insert_into_table('Chromosomes', chro[i], cursor)
        # Insert into gene table, providing both the gene name and 'chr_id'
        variant[i]['gene_id'] = insert_into_table('Genes', gene[i], cursor)
        insert_into_table('Variants', variant[i], cursor)


# MAIN
def main(args):
    '''
    using the argparse Python library we accept the following:
    Database name, Database username, Database password and an ANNOVAR annotation file.
    
    prints a small report that states the number of rows in each table.
    '''
    parser = argparse.ArgumentParser()
    parser.add_argument('database',
                        help = 'give a database name')
    parser.add_argument('username',
                        help = 'give a username')
    parser.add_argument('password',
                        help = 'give a password')
    parser.add_argument('ANNOVAR_file',
                        help = 'give a ANNOVAR file')


    args = parser.parse_args()

    cnx = database_connector(args.database, args.username, args.password)
    cursor = cnx.cursor()

    if check_annovar_file(args.ANNOVAR_file) == 1:
        create_tables(cursor)
        [variant, gene, chro] = read_annovar_file(args.ANNOVAR_file)
        fill_tables(variant, gene, chro, cursor)
        cnx.commit()

        print('Done. ALl information inserted into the database.', '\n')
        print('Number of rows in the Chromosomes table:', len(chro))
        print('Number of rows in the Genes table:', len(gene))
        print('Number of rows in the Variants table:', len(variant))
    else:
        print('there is a problem with your ANNOVAR file')

    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv))
